//===- Hello.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "Hello World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#include "llvm/ADT/Statistic.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/ScalarEvolutionAliasAnalysis.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/User.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include <set>
#include <vector>

using namespace llvm;

#define DEBUG_TYPE "hello"

STATISTIC(HelloCounter, "Counts number of functions greeted");

namespace {
// Hello - The first implementation, without getAnalysisUsage.
struct Hello : public FunctionPass {
  static char ID; // Pass identification, replacement for typeid
  Hello() : FunctionPass(ID) {}

  bool runOnFunction(Function &F) override {
    ++HelloCounter;
    errs() << "Hello: ";
    errs().write_escaped(F.getName()) << '\n';

    // if (F.getName() == "__ctt_error") {
    //      return false;
    // }

    //    for (BasicBlock &BB : F)
    // Print out the name of the basic block if it has one, and then the
    //   // number of instructions that it contains
    //	       errs() << "Basic block (name=" << BB.getName() << ") has "
    //	                    << BB.size() << " instructions.\n";
    //  uint16_t max = 0;
    //   StringRef name;
    //    for (BasicBlock &BB : F)
    //	      if(BB.size() > max){
    //		      max = BB.size();
    //		      name = BB.getName();
    //	      }
    //  errs() << "Basic block with greater = " <<name << " with size = "<<max
    //  << " \n";

    // prdecessor
    //  for (BasicBlock &BB : F){
    //  errs() << "Basic block = "<<BB.getName()<<"\n";
    //   for (BasicBlock *Pred : predecessors(&BB)) {
    //       errs()<<"Predecessor "<<Pred->getName().str()<<" Size =  "
    //       <<Pred->size()<<"\n";
    //   }
    //  }

    // sucessor
    //  for (BasicBlock &BB : F){
    //   errs() << "Basic block = "<<BB.getName()<<"\n";
    //   for (BasicBlock *Succ : successors(&BB)) {
    //     errs()<<"Successor "<<Succ->getName().str()<<" Size =  "
    //     <<Succ->size()<<"\n";
    //  }
    // }

    /*     IRBuilder<> Builder(F.front().getFirstNonPHI());

        // GlobalVariable *gVar = createGlob(builder, "gVar",F,0);
        // Constant *const_gv = gVar->getInitializer();
        // ConstantInt* constInt = cast<ConstantInt>(const_gv);
        // errs()<<"Global "<<constInt->getZExtValue()<<"\n";
        // int cnt=0;

      //    for (Function::iterator b = F.begin(), be = F.end(); b != be; ++b) {
    //	      BasicBlock *BB = &*b;
    //	ConstantInt* count  = ConstantInt::get(F.getContext() , APInt(32,cnt));
    //	builder.SetInsertPoint(BB, BB->begin());
    //	builder.CreateStore(count, gVar);
      //     cnt = cnt + 1;
    //	errs()<<"Global Value : "<<count->getZExtValue()<<"\n";
    //	Value *V = builder.CreateLoad(gVar);
    //	Value *U = builder.CreateXor(V,count);
            //ConstantInt* CI = dyn_cast<ConstantInt*>(*U);
    //	errs()<<"Value : "<<*U<<"\n";

    //     }



            int BlockCnt = 1,PredBlock,MultiPredBlockCount,Dblock;
            std::map<BasicBlock *,int>MapGi;
            std::map<BasicBlock *,int>MapDi;

            GlobalVariable *Gi = createGlob(Builder, "Gi",F,0);
            GlobalVariable *Di = createGlob(Builder, "Di",F,0);

            auto zero = Builder.getInt32(0);
            Builder.CreateStore(zero, Gi);

            for(BasicBlock &BB: F){
                            PredBlock = 0;MultiPredBlockCount =0;Dblock=0;
                            IRBuilder<> BuilderBlock(BB.getFirstNonPHI());

                            MapGi[&BB]= BlockCnt;
                            BasicBlock* fi;
                            int PredCount = pred_size(&BB);
                            if(PredCount>1){
                                    for (BasicBlock *Pred : predecessors(&BB)) {
                                            if(MultiPredBlockCount>=1)
                                                    PredBlock =  Dblock ^
    MapGi[Pred];

                                            else if(MultiPredBlockCount==0)
                                            {
                                                    Dblock = MapGi[Pred];
                                                    fi = Pred;
                                            }
                                            MultiPredBlockCount+=1;
                                    }
                            }

                            auto ddi = MapGi[fi] ^ MapGi[&BB];
                            auto di = BuilderBlock.getInt32(ddi);
                            Value* GlobalGi = BuilderBlock.CreateLoad(Gi);
                            auto GiDi = BuilderBlock.CreateXor(GlobalGi,di);

                            if(PredCount>1){
                                    auto PredVal =
    BuilderBlock.getInt32(PredBlock); BuilderBlock.CreateStore(PredVal, Di);
                                    auto Dia = BuilderBlock.CreateLoad(Di);
                                    GiDi = BuilderBlock.CreateXor(GiDi,Dia);
                            }

                            BuilderBlock.CreateStore(GiDi,Gi);
                            BlockCnt = BlockCnt + 1;

                            llvm::Function *ErrorFunc =
    F.getParent()->getFunction("__ctt_error"); if (ErrorFunc) { auto MapValue =
    BuilderBlock.getInt32(MapGi[&BB]); Value *arg[] = {MapValue,GiDi};
                                            llvm::SmallVector<Value*,20>
    ErrorFuncArgs; ErrorFuncArgs.push_back(MapValue);
                                            ErrorFuncArgs.push_back(GiDi);
                                            BuilderBlock.CreateCall(ErrorFunc,arg);
                            }

            }
        */

    return false;
  }

  /*  GlobalVariable *createGlob(IRBuilder<> &Builder, std::string Name,Function
     &F,int num) { F.getParent()->getOrInsertGlobal(Name, Builder.getInt32Ty());
                GlobalVariable *gVar = F.getParent()->getNamedGlobal(Name);
                gVar->setLinkage(GlobalValue::CommonLinkage);
                gVar->setAlignment(Align(4));
                gVar->setInitializer(ConstantInt::get(F.getParent()->getContext(),
     APInt(32, num, true))); return gVar;
          }
  */
};
} // namespace

char Hello::ID = 0;
static RegisterPass<Hello> X("hello", "Hello World Pass");

namespace {
// Hello2 - The second implementation with getAnalysisUsage implemented.
struct Hello2 : public FunctionPass {
  static char ID; // Pass identification, replacement for typeid
  Hello2() : FunctionPass(ID) {}

  bool runOnFunction(Function &F) override {
    ++HelloCounter;
    errs() << "Hello: ";
    errs().write_escaped(F.getName()) << '\n';

    ScalarEvolution *SE = &getAnalysis<ScalarEvolutionWrapperPass>().getSE();
    auto &LI = getAnalysis<LoopInfoWrapperPass>().getLoopInfo();
    //      SmallVector<const SCEV *,20>SCEVOrder;
    //      const SCEV* ArrayBaseStore;
    //      const SCEV* ArrayBaseLoad;
    //      std::map<const SCEV*,Instruction *>SCEVMap;
    //      Instruction *Inst;

    //    for(Loop *L : LI){
    //	      for (BasicBlock *BB : L->getBlocks()){
    //		      for (auto I = BB->begin(); I != BB->end(); ++I) {
    //			      if(isa<LoadInst>(I)){
    //				      const SCEV * Expr =
    //SE->getSCEV(getLoadStorePointerOperand(&*I)); 			     	      ArrayBaseLoad = Expr;
    //			      }
    //			      else if(isa<StoreInst>(I)){
    //			      	      const SCEV * Expr =
    //SE->getSCEV(getLoadStorePointerOperand(&*I)); 			      	      ArrayBaseStore = Expr;
    //			      }
    //		      }
    //	      }
    //    }
    //
    //    for(Loop *L : LI){
    //	      for (BasicBlock *BB : L->getBlocks()){
    //		      for (auto I = BB->begin(); I != BB->end(); ++I) {
    //			      if(isa<StoreInst>(I)){
    //				      const SCEV * Expr =
    //SE->getSCEV(getLoadStorePointerOperand(&*I)); 				      const SCEV* SCEVIndex =
    //SE->getMinusSCEV(Expr,ArrayBaseStore); 			      	      SCEVOrder.push_back(SCEVIndex);
    //			      	      SCEVMap[SCEVIndex]=&*I;
    //			      }
    //		      }
    //	      }
    //      }
    //  std::sort(SCEVOrder.begin(),SCEVOrder.end());
    //    for(auto Op=SCEVOrder.begin();Op!= SCEVOrder.end();Op++){
    //	      Inst = SCEVMap[*Op];
    //	      Op++;
    //	      if(SCEVMap[*Op])
    //	      SCEVMap[*Op]->moveAfter(Inst);
    //	      Op--;
    //      }

    return true;
  }

  // We don't modify the program, so we preserve all analyses.
  void getAnalysisUsage(AnalysisUsage &AU) const override {
    AU.setPreservesAll();
    AU.addRequired<ScalarEvolutionWrapperPass>();
    AU.addRequired<LoopInfoWrapperPass>();
  }
};
} // namespace

char Hello2::ID = 0;
static RegisterPass<Hello2>
    Y("hello2", "Hello World Pass (with getAnalysisUsage implemented)");
